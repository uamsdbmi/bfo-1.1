# README #


### What is this repository for? ###

* The IFOMIS site that hosts BFO 1.1 has proven unreliable over the past 6-12 months (dated from 2014-09), so the purpose of this site is to give ontology developers an alternative, more-reliable place to import BFO 1.1.  Because BFO 1.1 will not change, a copy may reside anywhere.  The only limitation is that software like OWL API and Protege (which uses OWL API) throws an error when importing a single ontology URI from two different web-based locations.  Therefore, we slightly modified the ontology IRI.
* BFO 1.1

* Summary of set up
Just import the link to the RAW BFO 1.1 file contained in this repository. Because we will not modify it further, that link will be stable.

* Repo owner or admin
You can talk to either Josh Hanna or Bill Hogan about it.  We recognize that the decision to put BFO 1.1 here will engender some controversy.  But we have been delayed and stymied and just outright frustrated in getting urgent work done due to the unreliability of the IFOMIS server.